// function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
// }

// Input [1,2,3,4,5,5]
function find(items,cb) {

    if(!Array.isArray(items) || !cb || !items) {  

        return undefined;
    } 

    for(let i = 0; i < items.length; i++) {

        let item=items[i];

        if(cb(item,i,items)) {
            
            return item;
        }
    }
    return undefined;
}
module.exports=find;
