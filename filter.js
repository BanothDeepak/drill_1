// function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
// }

// const items = [1, 2, 3, 4, 5, 5];


function filterFunction(items,cb) {

    if(!Array.isArray(items) || !cb || !items) {
        
        return undefined;
    }

    let arr=[];
    for(let i = 0; i < items.length; i++) {

        let item=items[i];

        if(cb(item,cb,items)){

            arr.push(item);
        }
    }
    return arr;
}
module.exports=filterFunction;
