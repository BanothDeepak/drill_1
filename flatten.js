// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

// function flatten(elements) {
     // Flattens a nested array (the nesting can be to any depth).
     // Hint: You can solve this using recursion.
     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }

function flattenFunction(nestedArray) {

    if (!Array.isArray(nestedArray) || !nestedArray ) {
      return [];
    }
    let i =[...nestedArray];
    const arr = [];
    while(i.length) { 

        let element = i.pop(); 

        if(Array.isArray(element)) { 

            i.push(...element);
        }else{
            arr.push(element);
        }
    }
    return arr.reverse();
}
module.exports=flattenFunction;
