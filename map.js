//const items = [1, 2, 3, 4, 5, 5];


function map(items, cb) {

    if( !Array.isArray(items) || !cb || !items ) {
        console.log(undefined);
    }
    let resultArray = [];

    for(let i = 0; i < items.length; i++) {

        let list = items[i];

        let cbOuput = cb(list, i, items);
        
        resultArray.push(cbOuput);
    }
    console.log(resultArray);
}

module.exports=map;