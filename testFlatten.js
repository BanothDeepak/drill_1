
let nestedArray = [1, [2], [[3]], [[[4]]]];

let flattenFunction = require('./flatten');

const res = flattenFunction(nestedArray);

console.log(res);
